Backing storage
The Jaeger Collector and Query require a backing storage to exist before being started up. As a starting point for your own templates, we provide basic templates deploying Cassandra and Elasticsearch. None of them are ready for production and should be adapted before any real usage.

kubectl create -f https://raw.githubusercontent.com/jaegertracing/jaeger-kubernetes/master/production/configmap.yml
kubectl create -f https://raw.githubusercontent.com/jaegertracing/jaeger-kubernetes/master/production/cassandra.yml

Even though this template uses a stateful Cassandra, backing storage is set to emptyDir. It's more appropriate to create a PersistentVolumeClaim/PersistentVolume and use it instead.

kubectl get job jaeger-cassandra-schema-job

The job should have 1 in the SUCCESSFUL column.

Jaeger components
The main production template deploys the Collector and the Query Service (with UI) as separate individually scalable services, as well as the Agent as DaemonSet.

kubectl create -f https://raw.githubusercontent.com/jaegertracing/jaeger-kubernetes/master/jaeger-production-template.yml

Deploying the agent as sidecar
The Jaeger Agent is designed to be deployed local to your service, so that it can receive traces via UDP keeping your application's load minimal. By default, the template above installs the agent as a DaemonSet, but this means that all pods running on a given node will send data to the same agent. If that's not suitable for your workload, an alternative is to deploy the agent as a sidecar. To accomplish that, just add it as a container within any struct that supports spec.containers, like a Pod, Deployment and so on

- apiVersion: apps/v1
  kind: Deployment
  metadata:
  name: myapp
  spec:
  selector:
  matchLabels:
  app: myapp
  template:
  metadata:
  labels:
  app: myapp
  spec:
  containers: - image: mynamespace/hello-myimage
  name: myapp
  ports: - containerPort: 8080 - image: jaegertracing/jaeger-agent
  name: jaeger-agent
  ports: - containerPort: 5775
  protocol: UDP - containerPort: 6831
  protocol: UDP - containerPort: 6832
  protocol: UDP - containerPort: 5778
  protocol: TCP
  args: ["--collector.host-port=jaeger-collector.jaeger-infra.svc:14267"]

Helm Chart

https://github.com/helm/charts/tree/master/incubator/jaeger
